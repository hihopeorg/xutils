/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export class File {
    // multipart提交时，请求头中的文件名
    filename: string
    // multipart提交时，表单项目的名称，缺省为file
    name: string
    // 文件的本地存储路径
    //支持“dataability”和“internal”两种协议类型，但“internal”仅支持临时目录，示例：
    //dataability:///com.domainname.dataability.persondata/person/10/file.txt
    //internal://cache/path/to/file.txt
    uri: string
    // 文件的内容类型，默认根据文件名或路径的后缀获取
    type?: string
}