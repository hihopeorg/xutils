/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { UploadRequestData } from './UploadRequestData';

import request from '@ohos.request';

export class UploadConfig {
    // 资源地址
    url: string
    // 添加要包含在上载请求中的HTTP或HTTPS标志头
    header: object
    // 请求方法：POST、PUT。缺省为POST
    method: string
    // 要上传的文件列表。请使用 multipart/form-data提交
    files: Array<request.File>
    // 请求的表单数据
    data: Array<UploadRequestData>
}