/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import http from '@ohos.net.http';

export class ResponseInfo<T> {
    public result: T;
    public resultFormCache: boolean;
    public statusCode: number;
    public reasonPhrase: string;
    private response: http.HttpResponse;


    public constructor(response: http.HttpResponse, result: any, resultFormCache: boolean) {
        this.response = response;
        this.result = result;
        this.resultFormCache = resultFormCache;
    }
}
