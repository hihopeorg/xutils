/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


export abstract class QueryCallBack {
    public abstract onSuccessCreateDb();

    public abstract onSuccessCreateTable();

    public abstract onSuccessInsert(ret: any);

    public abstract onErrorInsert(err: any);

    public abstract onSuccessQuery(resultSet: any);

    public abstract onSuccessQueryFirst(map: Map<string, any>);

    public abstract onSuccessUpdate(ret: any);

    public abstract onErrorUpdate(err: any);

    public abstract onSuccessDelete(rows: any);

    public abstract onErrorDelete(err: any);

    public abstract onSuccessDropTable();

    public abstract onSuccessDeleteDb();
}
