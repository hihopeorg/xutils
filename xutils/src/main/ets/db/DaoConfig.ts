/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { DbUpgradeListener } from './DbUpgradeListener'

export class DaoConfig {
    private dbName = "xUtils.db"; // default db name
    private tableName: string;
    private dbVersion = 1;
    private dbUpgradeListener: DbUpgradeListener;
    private dbDir: string;
    private createTableSql: string;

    public getDbName(): string {
        return this.dbName;
    }

    public setDbName(dbName: string) {
        this.dbName = dbName;
    }

    public getTableName(): string {
        return this.tableName;
    }

    public setTableName(tableName: string) {
        if (tableName != null) {
            this.tableName = tableName;
        }
    }

    public getDbVersion(): number {
        return this.dbVersion;
    }

    public setDbVersion(dbVersion: number) {
        this.dbVersion = dbVersion;
    }

    public getDbUpgradeListener(): DbUpgradeListener {
        return this.dbUpgradeListener;
    }

    public setDbUpgradeListener(dbUpgradeListener: DbUpgradeListener) {
        this.dbUpgradeListener = dbUpgradeListener;
    }

    public getDbDir(): string {
        return this.dbDir;
    }

    public setDbDir(dbDir: string) {
        this.dbDir = dbDir;
    }

    public getCreateTableSql(): string {
        return this.createTableSql;
    }

    public setCreateTableSql(createTableSql: string) {
        this.createTableSql = createTableSql;
    }
}