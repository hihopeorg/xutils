/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { TaskHandler } from './TaskHandler'
import { Priority } from './Priority'
import { HttpRequest } from '../http/client/HttpRequest'
import worker from '@ohos.worker';

export abstract class BitMapTask implements TaskHandler {
    sHandler = new worker.Worker("workers/worker.js", { name: "first worker" });
    private priority: Priority
    private mCancelled = false
    private MESSAGE_POST_RESULT: number = 0x1;
    private MESSAGE_POST_PROGRESS: number = 0x2;
    private mExecuteInvoked: boolean = false;

    setPriority(priority: Priority) {
        this.priority = priority;
    }

    getPriority() {
        return this.priority;
    }

    public PriorityAsyncTask() {

    }

    supportPause(): boolean {
        return false
    }

    supportResume(): boolean {
        return false
    }

    supportCancel(): boolean {
        return true
    }

    isPaused(): boolean {
        return false
    }

    isCancelled(): boolean {
        return this.mCancelled
    }

    pause() {

    }

    resume() {
    }

    cancel() {
        this.canceled(true);
    }

    public canceled(mayInterruptIfRunning: boolean): boolean {
        this.mCancelled = true;
        return true;
    }

    public executeOnExecutor(params: any) {
        this.doInBackground(params)
        this.mExecuteInvoked = true
    }

    public executeOnExecutorDownload(params: HttpRequest, target: string, autoResume: boolean, autoRename: boolean) {
    }

    abstract doInBackground(params: any);

    publishProgress(values: any) {
        if (!this.isCancelled()) {
            this.sHandler.postMessage(this.MESSAGE_POST_PROGRESS)
        }
    };

    protected onProgressUpdate(values: object) {
    }

    protected onPostExecute(result: number, uri?: string) {
    }
}
