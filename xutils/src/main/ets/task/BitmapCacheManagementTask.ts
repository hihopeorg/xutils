/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { BitMapTask } from '../task/bitmaptask';
import { Priority } from '../task/priority'

export class BitmapCacheManagementTask extends BitMapTask {
    public MESSAGE_INIT_MEMORY_CACHE: number = 0;
    public MESSAGE_INIT_DISK_CACHE: number = 1;
    public MESSAGE_FLUSH: number = 2;
    public MESSAGE_CLOSE: number = 3;
    public MESSAGE_CLEAR: number = 4;
    public MESSAGE_CLEAR_MEMORY: number = 5;
    public MESSAGE_CLEAR_DISK: number = 6;
    public MESSAGE_CLEAR_BY_KEY: number = 7;
    public MESSAGE_CLEAR_MEMORY_BY_KEY: number = 8;
    public MESSAGE_CLEAR_DISK_BY_KEY: number = 9;
    private val: object;
    private mContext: any = null;
    private path = "";

    constructor(context: any) {
        super()
        this.mContext = context;
    }

    setDiskPath(path: string) {
        this.path = path;
    }

    doInBackground(params: object): object {
        return params;
    }

    onPostExecute(params: number, uri?: string): void {
        if (params == null) {
            return
        } else {
            if (this.mContext.bitmapCacheListener == null) {
                let cache = this.mContext.getBitmapCache();
                if (cache == null) return;
                try {
                    switch (params) {
                        case this.MESSAGE_INIT_MEMORY_CACHE:
                            cache.initMemoryCache();
                            break;
                        case this.MESSAGE_INIT_DISK_CACHE:
                            cache.initDiskCache();
                            break;
                        case this.MESSAGE_FLUSH:
                            cache.flush();
                            break;
                        case this.MESSAGE_CLOSE:
                            cache.clearMemoryCache();
                            cache.close();
                            break;
                        case this.MESSAGE_CLEAR:
                            cache.clearCache(uri);
                            break;
                        case this.MESSAGE_CLEAR_MEMORY:
                            cache.clearMemoryCache();
                            break;
                        case this.MESSAGE_CLEAR_DISK:
                            cache.clearDiskCache();
                            break;
                        case this.MESSAGE_CLEAR_BY_KEY:
                            if (uri.length != 2) return;
                            cache.clearCache();
                            break;
                        case this.MESSAGE_CLEAR_MEMORY_BY_KEY:
                            if (uri.length != 2) return;
                            cache.clearMemoryCache(uri);
                            break;
                        case this.MESSAGE_CLEAR_DISK_BY_KEY:
                            if (uri.length != 2) return;
                            cache.clearDiskCache();
                            break;
                        default:
                            break;
                    }
                } catch (e) {
                    console.error(e)
                }
            } else {
                try {
                    switch (params) {
                        case this.MESSAGE_INIT_MEMORY_CACHE:
                            this.mContext.getBitmapCacheListener().onInitMemoryCacheFinished();
                            break;
                        case this.MESSAGE_INIT_DISK_CACHE:
                            this.mContext.getBitmapCacheListener().onInitDiskFinished();
                            break;
                        case this.MESSAGE_FLUSH:
                            this.mContext.getBitmapCacheListener().onFlushCacheFinished();
                            break;
                        case this.MESSAGE_CLOSE:
                            this.mContext.getBitmapCacheListener().onCloseCacheFinished();
                            break;
                        case this.MESSAGE_CLEAR:
                            this.mContext.getBitmapCacheListener().onClearCacheFinished();
                            break;
                        case this.MESSAGE_CLEAR_MEMORY:
                            this.mContext.getBitmapCacheListener().onClearMemoryCacheFinished();
                            break;
                        case this.MESSAGE_CLEAR_DISK:
                            this.mContext.getBitmapCacheListener().onClearDiskCacheFinished();
                            break;
                        case this.MESSAGE_CLEAR_BY_KEY:
                            if (uri.length != 2) return;
                            this.mContext.getBitmapCacheListener().onClearCacheFinished(params);
                            break;
                        case this.MESSAGE_CLEAR_MEMORY_BY_KEY:
                            if (uri.length != 2) return;
                            this.mContext.getBitmapCacheListener().onClearMemoryCacheFinished(params);
                            break;
                        case this.MESSAGE_CLEAR_DISK_BY_KEY:
                            if (uri.length != 2) return;
                            this.mContext.getBitmapCacheListener().onClearDiskCacheFinished(params);
                            break;
                        default:
                            break;
                    }
                } catch (e) {
                    console.error(e)
                }
            }
        }

    }

    private BitmapCacheManagementTask() {
        this.setPriority(Priority.UI_TOP);
    }
}

