/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { BitmapLoadCallBack } from '../bitmap/callback/bitmaploadcallback';
import { BitmapDisplayConfig } from '../bitmap/bitmapdisplayconfig'
import { BitmapLoadFrom } from '../bitmap/callback/bitmaploadfrom'
import { PriorityAsyncTask } from './PriorityAsyncTask'
import { BitmapGlobalConfig } from '../bitmap/BitmapGlobalConfig'

export class BitmapLoadTask extends PriorityAsyncTask {
    public uri: string;
    private displayConfig: BitmapDisplayConfig = null;
    private mfrom: BitmapLoadFrom = BitmapLoadFrom.DISK_CACHE;
    private PROGRESS_LOAD_STARTED = 0;
    private PROGRESS_LOADING = 1;
    private callBack: BitmapLoadCallBack
    private globalConfig: BitmapGlobalConfig;

    constructor(uri: string, config: BitmapDisplayConfig, callBack: BitmapLoadCallBack, globalConfig: BitmapGlobalConfig) {
        super()
        this.callBack = callBack;
        this.uri = uri;
        this.displayConfig = config;
        this.globalConfig = globalConfig;
    }

    doInBackgroundForPixelMap() {
        //从本地获取图片
        this.globalConfig.getBitmapCache()
            .downloadBitmap(this.uri, <any> this.displayConfig, this, <any> this.callBack);
    }

    doInBackground(params: object): any {
        return null
    }

    isCancelled(): boolean {
        return false;
    }

    onProgressUpdate(value: object) {
        if (value == null) return;
        switch (value[0]) {
            case this.PROGRESS_LOAD_STARTED:
                this.callBack.onLoadStarted(this.uri, <any> this.displayConfig)
                break;
        }
    }
    doInBackgroundDownload() {
    }

    onPostExecute(pixMap) {
    }

    onCancelled(pix) {
    }
}

