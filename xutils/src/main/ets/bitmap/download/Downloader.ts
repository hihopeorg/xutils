/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export abstract class Downloader {
    context: any;
    defaultExpiry: number;
    defaultConnectTimeout: number;
    defaultReadTimeout: number;

    /**
     * Download bitmap to outputStream by uri.
     *
     * @param uri
     * @param outputStream
     * @return The expiry time stamp or -1 if failed to download.
     */
    abstract downloadToStream(uri: string, outputStream: any): number;

    getContext(): any {
        return this.context;
    }

    setContext(context: any) {
        this.context = context;
    }

    setDefaultExpiry(expiry: number) {
        this.defaultExpiry = expiry;
    }

    getDefaultExpiry(): number {
        return this.defaultExpiry;
    }

    getDefaultConnectTimeout(): number {
        return this.defaultConnectTimeout;
    }

    setDefaultConnectTimeout(defaultConnectTimeout: number) {
        this.defaultConnectTimeout = defaultConnectTimeout;
    }

    getDefaultReadTimeout(): number {
        return this.defaultReadTimeout;
    }

    setDefaultReadTimeout(defaultReadTimeout: number) {
        this.defaultReadTimeout = defaultReadTimeout;
    }
}
