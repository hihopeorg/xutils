## v1.0.1  
  1. 适配DevEco Studio 3.1Beta1及以上版本
  2. fs替换fileio读写文件

## v1.0.0

- 已实现功能
  1. 数据请求
  2. 文件下载
  3. 文件上传
  4. 创建数据库
  5. 创建表
  6. 插入数据
  7. 查询数据
  8. 更新数据
  9. 删除数据
  10. 删除表
  11. 删除数据库
  12. 加载图片
  13. 从缓存获取图片
  14. 清除本地文件
  15. 网络重连，请求拦截器，Gzip压缩，https处理

- 未实现功能

  1.ssl安全连接和useragent配置无法实现，现在ETS的request接口支持的选项，HttpRequestOptions中暂时没有key、cert、ca、rejectUnauthorized等属性参数